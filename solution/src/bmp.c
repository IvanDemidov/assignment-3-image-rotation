#include <stdio.h>
#include <stdlib.h>

#include "bmp_header.h"
#include "util.h"

enum read_status from_bmp(FILE* file, struct image* image){
    struct bmp_header bmp_header;
    size_t read_count = fread(&bmp_header, sizeof(bmp_header), 1, file);
    if (read_count < 1) return INVALID_HEADER;

    if ((bmp_header.bfType != 0x4D42)
        || (bmp_header.biSizeImage && bmp_header.bfileSize != bmp_header.bOffBits + bmp_header.biSizeImage)
        || (bmp_header.biPlanes != 1)
        || (bmp_header.biBitCount != 24)
        || (bmp_header.biCompression != 0)) {
        return INVALID_SIGNATURE;
    }

    if (fseek(file, 0L, SEEK_END)
        || ftell(file) != bmp_header.bfileSize
        || fseek(file, bmp_header.bOffBits, SEEK_SET)) {
        return INVALID_FILE;
    }

    image->width = bmp_header.biWidth;
    image->height = bmp_header.biHeight;
    image->data = malloc(sizeof(struct pixel) * bmp_header.biWidth * bmp_header.biHeight);
    if (image->data == NULL) return INVALID_MALLOC;

    uint32_t real_width = image->width * sizeof(struct pixel);
    int64_t padding =  (int64_t) (4 - (real_width % 4));
    if (padding == 4) padding = 0;

    for (int32_t tmp_height = 0; tmp_height < image->height; tmp_height++) {
        read_count = fread(image->data + tmp_height * image->width, real_width, 1, file);
        if (read_count != 1) return READ_ERROR;

        fseek(file, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* file, struct image* image){
    struct bmp_header bmp_header = make_bmp_header(image);
    size_t write_count = fwrite(&bmp_header, sizeof(struct bmp_header), 1, file);

    if (write_count < 1) return WRITE_HEADER_ERROR;

    uint64_t real_width = image->width * sizeof(struct pixel);
    uint64_t padding = 4 - (real_width % 4);
    if (padding == 4) padding = 0;

    for (int32_t tmp_height = 0; tmp_height < image->height; tmp_height++) {
        write_count = fwrite(image->data + tmp_height * image->width, real_width, 1, file);

        if (write_count < 1) return WRITE_DATA_ERROR;

        struct pixel pixel = {.b = 0, .g = 0, .r = 0};
        write_count = fwrite(&pixel, 1, padding, file);

        if (write_count < 1) return WRITE_PADDING_ERROR;
    }

    return WRITE_OK;
}
