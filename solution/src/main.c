#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "io_file.h"
#include "transform.h"
#include "util.h"

const char* const READ_STATUSES[] = {
        [READ_OK] = "OK\n",
        [INVALID_HEADER] = "Error reading the header!\n",
        [INVALID_SIGNATURE] = "Error reading the header arguments!\n",
        [INVALID_FILE] = "Error reading the file!\n",
        [INVALID_MALLOC] = "Error allocating memory for an image!\n",
        [READ_ERROR] = "Error reading pixels!\n"
};

const char* const WRITE_STATUSES[] = {
        [WRITE_OK] = "OK\n",
        [WRITE_HEADER_ERROR] = "Error writing the header!\n",
        [WRITE_DATA_ERROR] = "Error writing the pixels!\n",
        [WRITE_PADDING_ERROR] = "Error writing the paddings!\n"
};

int main(int argc, char** argv) {
    if (argc != 4){
        printf("Incorrect number of arguments!\n");
        printf("Follow the format:\"./image-transformer <source-image> <transformed-image> <angle>\"\n");
        return 0;
    }

    char* flag;
    int64_t angle = ((int64_t) strtol(argv[3], &flag, 10) + 360) % 360;
    if (flag == NULL){
        printf("Incorrect angle value!\n");
        return 0;
    } else {
        if (angle % 90 != 0){
            printf("Incorrect angle value!\n");
            printf("The angle value must be one of the {0, 90, -90, 180, -180, 270, -270}\n");
            return 0;
        }
    }

    struct image image;

    FILE* source_file = open_read_byte_file(argv[1]);
    if (source_file == NULL){
        printf("Error opening the source file!\n");
        return 0;
    }
    enum read_status read_status = from_bmp(source_file, &image);
    if (read_status != READ_OK){
        printf("%s\n", READ_STATUSES[read_status]);
        return 0;
    }
    int8_t x = close_file(source_file);
    if (x == EOF){
        printf("Error closing the source file!\n");
        return 0;
    }

    for (int8_t i = 0; i < (int8_t) (angle / 90); i++) {
        struct image new_image = transform(&image);
        if (new_image.height == 0 && new_image.width == 0 && new_image.data == NULL){
            printf("Error transforming the image!\n");
            return 0;
        }
        if (image.data != NULL){
            free(image.data);
            image.data = NULL;
        }
        image = new_image;
    }

    FILE* transformed_file = open_write_byte_file(argv[2]);
    enum write_status write_status = to_bmp(transformed_file, &image);
    if (write_status != WRITE_OK){
        printf("%s\n", WRITE_STATUSES[write_status]);
        return 0;
    }
    x = close_file(transformed_file);
    if (x == EOF){
        printf("Error closing the transformed file!\n");
        return 0;
    }

    if (image.data != NULL) free(image.data);

    printf("OK\n");
    return 0;
}
