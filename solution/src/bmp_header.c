#include "bmp_header.h"
#include "util.h"

struct bmp_header make_bmp_header(const struct image* image){
    uint64_t real_width = image->width * sizeof(struct pixel);
    uint64_t padding = 4 - (real_width % 4);
    if (padding == 4) padding = 0;
    uint64_t width_with_padding = real_width + padding;

    struct bmp_header bmp_header;
    bmp_header.bfType = 0x4D42;
    bmp_header.bfileSize = sizeof(struct bmp_header) + width_with_padding * image->height;
    bmp_header.bfReserved = 0;
    bmp_header.bOffBits = sizeof(struct bmp_header);
    bmp_header.biSize = 40;
    bmp_header.biWidth = image->width;
    bmp_header.biHeight = image->height;
    bmp_header.biPlanes = 1;
    bmp_header.biBitCount = 24;
    bmp_header.biCompression = 0;
    bmp_header.biSizeImage = width_with_padding * image->height;
    bmp_header.biXPelsPerMeter = 0;
    bmp_header.biYPelsPerMeter = 0;
    bmp_header.biClrUsed = 0;
    bmp_header.biClrImportant = 0;
    return bmp_header;
}
