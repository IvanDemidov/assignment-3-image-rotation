#include <stdint.h>
#include <stdio.h>

FILE* open_read_byte_file(const char* str){
    return fopen(str, "rb");
}

FILE* open_write_byte_file(const char* str){
    return fopen(str, "wb");
}

int8_t close_file(FILE* file){
    return (int8_t) fclose(file);
}
