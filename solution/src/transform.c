#include <stdio.h>
#include <stdlib.h>

#include "util.h"

struct image transform(struct image* image){
    struct image new_image;
    new_image.height = image->width;
    new_image.width = image->height;
    new_image.data = malloc(sizeof(struct pixel) * new_image.width * new_image.height);
    if (new_image.data == NULL) return (struct image) {.height = 0, .width = 0, .data = NULL};

    for (size_t i = 0; i < new_image.width; i++)
        for (size_t j = 0; j < new_image.height; j++)
            new_image.data[(new_image.height - (j + 1)) * new_image.width + i] = image->data[i * image->width + j];

    return new_image;
}
