#pragma once

#include <stdio.h>

FILE* open_read_byte_file(const char*);
FILE* open_write_byte_file(const char* str);
int8_t close_file(FILE*);
