#pragma once

#include <stdio.h>

#include "util.h"

enum read_status from_bmp(FILE*, struct image*);
enum write_status to_bmp(FILE*, struct image*);
