#pragma once

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

enum read_status {
    READ_OK = 0,
    INVALID_HEADER,
    INVALID_SIGNATURE,
    INVALID_FILE,
    INVALID_MALLOC,
    READ_ERROR
};

//char* READ_STATUSES[] = {};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR,
    WRITE_PADDING_ERROR
};

//char* WRITE_STATUSES[] = {};
